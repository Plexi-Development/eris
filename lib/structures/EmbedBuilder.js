const Util = require('../util/Util.js');

class MessageEmbed {
  constructor(data = {}) {
    this.setup(data);
  }


  setup(data) {
    this.type = 'rich';

    this.title = data.title;

    this.description = data.description;

    this.url = data.url;

    this.color = data.color ? data.timestamp : 0x36393e;

    this.timestamp = data.timestamp ? new Date(data.timestamp).getTime() : null;

    this.fields = data.fields ? data.fields.map(Util.cloneObject) : [];

    this.thumbnail = data.thumbnail ? {
      url: data.thumbnail.url,
      proxyURL: data.thumbnail.proxy_url,
      height: data.thumbnail.height,
      width: data.thumbnail.width,
    } : null;

    this.image = data.image ? {
      url: data.image.url,
      proxyURL: data.image.proxy_url,
      height: data.image.height,
      width: data.image.width,
    } : null;

    this.video = data.video;

    this.author = data.author ? {
      name: data.author.name,
      url: data.author.url,
      iconURL: data.author.iconURL || data.author.icon_url,
      proxyIconURL: data.author.proxyIconUrl || data.author.proxy_icon_url,
    } : null;

    this.provider = data.provider;

    this.footer = data.footer ? {
      text: data.footer.text,
      iconURL: data.footer.iconURL || data.footer.icon_url,
      proxyIconURL: data.footer.proxyIconURL || data.footer.proxy_icon_url,
    } : null;

    this.files = [];
    if (data.files) {
      this.files = data.files;
    }

  }

  attachFiles(files) {
    this.files = this.files.concat(files);
    return this;
  }


  addField(name, value, inline = false) {
    if (this.fields.length >= 25) throw new RangeError('EMBED_FIELD_COUNT');
    name = Util.resolveString(name);
    if (!String(name)) throw new RangeError('EMBED_FIELD_NAME');
    value = Util.resolveString(value);
    if (!String(value)) throw new RangeError('EMBED_FIELD_VALUE');
    this.fields.push({ name, value, inline });
    return this;
  }

  addBlankField(inline = false) {
    return this.addField('\u200B', '\u200B', inline);
  }

  setAuthor(name, icon_url, url) {
    this.author = { name: Util.resolveString(name), icon_url, url };
    return this;
  }

  setColor(color) {
    this.color = Util.resolveColor(color);
    return this;
  }

  setDescription(description) {
    description = Util.resolveString(description);
    this.description = description;
    return this;
  }

  setFooter(text, iconURL) {
    text = Util.resolveString(text);
    this.footer = { text, iconURL };
    return this;
  }

  setImage(url) {
    this.image = { url };
    return this;
  }

  setThumbnail(url) {
    this.thumbnail = { url };
    return this;
  }

  setTimestamp(timestamp = new Date()) {
    this.timestamp = timestamp;
    return this;
  }

  setTitle(title) {
    title = Util.resolveString(title);
    this.title = title;
    return this;
  }

  setURL(url) {
    this.url = url;
    return this;
  }
}

module.exports = MessageEmbed;
