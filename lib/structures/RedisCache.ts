import * as redis from 'ioredis';

export class RedisCache {
  public baseObj: any;
  public _driver: redis.Redis;
  public options: redis.RedisOptions;

  constructor(baseObj, connectionName = null) {
    this.baseObj = baseObj;
    if (!process.env.REDIS_HOST || !process.env.REDIS_PORT || !process.env.REDIS_IP_FAMILY) throw new Error('REDIS_HOST missing in process environment!');

    try {
      this.options = Object.apply({
        host: process.env.REDIS_HOST,
        port: parseInt(process.env.REDIS_PORT),
        family: process.env.REDIS_IP_FAMILY,
        connectionName
      });

      this._driver = new redis(this.options);

    } catch (e) {
      console.error(e)
    }
  }

  get driver() {
    return this._driver
  }

  async add(obj: any, extra: any) {
    if (obj.id == null) {
      throw new Error('Missing object id');
    }
    const existing = await this.driver.get(obj.id);
    if (existing) return JSON.parse(existing);
    if (!(obj instanceof this.baseObj || obj.constructor.name === this.baseObj.name)) {
      obj = new this.baseObj(obj, extra);
    }
    await this.driver.set(obj.id, typeof obj == 'object' ? JSON.stringify(obj) : obj);
    return obj;
  }

  get(key: string) {
    return this._driver.get(key)
  }

  set(key: string, value: any) {
    return this._driver.set(key, value)
  }
}