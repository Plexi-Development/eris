const { Colors } = require('../Constants');
class Util {
  constructor() {
    throw new Error(`The ${this.constructor.name} class may not be instantiated.`);
  }

  static resolveString(data) {
    if (typeof data === 'string') return data;
    if (data instanceof Array) return data.join('\n');
    return String(data);
  }

  static cloneObject(obj) {
    return Object.assign(Object.create(obj), obj);
  }

  static resolveColor(color) {
    if (typeof color === 'string') {
      if (color === 'RANDOM') return Math.floor(Math.random() * (0xFFFFFF + 1));
      if (color === 'DEFAULT') return 0;
      color = Colors[color] || parseInt(color.replace('#', ''), 16);
    } else if (color instanceof Array) {
      color = (color[0] << 16) + (color[1] << 8) + color[2];
    }

    if (color < 0 || color > 0xFFFFFF) throw new RangeError('COLOR_RANGE');
    else if (color && isNaN(color)) throw new TypeError('COLOR_CONVERT');

    return color;
  }

  static resolveTime(time) {
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = time % 60;
    var ret = '';
    if (hrs > 0) {
      ret += '' + hrs + ':' + (mins < 10 ? '0' : '');
    }
    ret += '' + mins + ':' + (secs < 10 ? '0' : '');
    ret += '' + secs;
    return ret;
  }


  /**
   * Flatten an object. Any properties that are collections will get converted to an array of keys.
   * @param {Object} obj The object to flatten.
   * @param {...Object<string, boolean|string>} [props] Specific properties to include/exclude.
   * @returns {Object}
   */
  static flatten(obj, ...props) {
    const isObject = d => typeof d === 'object' && d !== null;
    if (!isObject(obj)) return obj;

    props = Object.assign(...Object.keys(obj).filter(k => !k.startsWith('_')).map(k => ({ [k]: true })), ...props);

    const out = {};

    for (let [prop, newProp] of Object.entries(props)) {
      if (!newProp) continue;
      newProp = newProp === true ? prop : newProp;

      const element = obj[prop];
      const elemIsObj = isObject(element);
      const valueOf = elemIsObj && typeof element.valueOf === 'function' ? element.valueOf() : null;

      // If it's a collection, make the array of keys
      if (element instanceof require('./Collection')) out[newProp] = Array.from(element.keys());
      // If it's an array, flatten each element
      else if (Array.isArray(element)) out[newProp] = element.map(e => Util.flatten(e));
      // If it's an object with a primitive `valueOf`, use that value
      else if (valueOf && !isObject(valueOf)) out[newProp] = valueOf;
      // If it's a primitive
      else if (!elemIsObj) out[newProp] = element;
    }

    return out;
  }


  static getUptime() {
    var d = Math.floor(process.uptime() / 86400);
    var hrs = Math.floor((process.uptime() % 86400) / 3600);
    var min = Math.floor(((process.uptime() % 86400) % 3600) / 60);
    var sec = Math.floor(((process.uptime() % 86400) % 3600) % 60);

    if (d === 0 && hrs !== 0) {
      return `${hrs} Hour(s), ${min} Minute(s) & ${sec} Second(s)`;
    } else if (d === 0 && hrs === 0 && min !== 0) {
      return `${min} Minute(s) & ${sec} Second(s)`;
    } else if (d === 0 && hrs === 0 && min === 0) {
      return `${sec} Second(s)`;
    } else {
      return `${d} Day, ${hrs} Hour(s), ${min} Minute(s) & ${sec} Second(s)`;
    }
  }

  static isNil(value) {
    return value == null;
  }
}

module.exports = Util;
